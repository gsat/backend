using System;

namespace Backend.Models
{
    public class PlayerInfo
    {
        public int UserId { get; set; }
        
        public Guid? Guid { get; set; }
        
        public string Name { get; set; }
        
        public DateTime Connected { get; set; }
        
        public DateTime? Disconnected { get; set; }
        
        public TimeSpan? SessionTime { get; set; }
        
        public int Ping { get; set; }
        
        public int Loss { get; set; }
        
        public string IpAddress { get; set; }
        
        public int Port { get; set; }
        
        public int Kills { get; set; }
        
        public int Deaths { get; set; }
        
        public PlayerHitGroup HitGroup { get; set; } = new PlayerHitGroup();
        
        public string Country { get; set; }
    }
}
using System.Collections.Generic;

namespace Backend.Models
{
    public class MapCollections
    {
        /// <summary>
        ///  awp_*.bsp maps
        /// </summary>
        public List<string> SniperRifle { get; set; } = new List<string>();
        
        /// <summary>
        ///  de_*.bsp maps
        /// </summary>
        public List<string> BombDefusal { get; set; } = new List<string>();
        
        /// <summary>
        ///  cs_*.bsp maps
        /// </summary>
        public List<string> HostageRescue { get; set; } = new List<string>();
        
        /// <summary>
        ///  Unknown map prefix
        /// </summary>
        public List<string> Another { get; set; } = new List<string>();
    }
}
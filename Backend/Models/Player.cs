using System;

namespace Backend.Models
{
    public class Player
    {
        public int UserId { get; set; }
        
        public string UserName { get; set; }
        
        public TimeSpan PlayTime { get; set; }
        
        public int Ping { get; set; }
        
        public int Loss { get; set; }
        
        public string Ip { get; set; }
        
        public int Port { get; set; }
    }
}
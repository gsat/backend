namespace Backend.Models
{
    public class PlayerSay
    {
        public int UserId { get; set; }
        
        public string Name { get; set; }
        
        public string Message { get; set; }
    }
}
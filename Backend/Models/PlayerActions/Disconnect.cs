namespace Backend.Models.PlayerActions
{
    public class Disconnect
    {
        public string Name { get; set; }
        
        public int UserId { get; set; }
        
        public string Reason { get; set; }
    }
}
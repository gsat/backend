namespace Backend.Models.PlayerActions
{
    public class Say
    {
        public string Name { get; set; }
        
        public int UserId { get; set; }
        
        public string Message { get; set; }
    }
}
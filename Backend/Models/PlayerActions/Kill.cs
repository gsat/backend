namespace Backend.Models.PlayerActions
{
    public class Kill
    {
        public string SrcName { get; set; }
        
        public int SrcUserId { get; set; }
        
        public string DstName { get; set; }
        
        public int DstUserId { get; set; }
        
        public string WithWeapon { get; set; }
    }
}
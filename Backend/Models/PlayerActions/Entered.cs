using System;

namespace Backend.Models.PlayerActions
{
    public class Entered
    {
        public string Name { get; set; }
        
        public int UserId { get; set; }
        
        public Guid Guid { get; set; }
    }
}
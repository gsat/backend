namespace Backend.Models.PlayerActions
{
    public class Hurt
    {
        public string SrcName { get; set; }
        
        public int SrcUserId { get; set; }
        
        public string DstName { get; set; }
        
        public int DstUserId { get; set; }
        
        // Default damage
        
        public int Generic { get; set; }
        
        public int Head { get; set; }
        
        public int Chest { get; set; }
        
        public int Stomach { get; set; }
        
        public int LeftHand { get; set; }
        
        public int RightHand { get; set; }
        
        public int LeftLeg { get; set; }
        
        public int RightLeg { get; set; }
        
        public int Gear { get; set; }
        
        // Armor damage
        
        public int GenericArmor { get; set; }
        
        public int HeadArmor { get; set; }
        
        public int ChestArmor { get; set; }
        
        public int StomachArmor { get; set; }
        
        public int LeftHandArmor { get; set; }
        
        public int RightHandArmor { get; set; }
        
        public int LeftLegArmor { get; set; }
        
        public int RightLegArmor { get; set; }
        
        public int GearArmor { get; set; }
    }
}
namespace Backend.Models.PlayerActions
{
    public class Connect
    {
        public string Name { get; set; }
        
        public int UserId { get; set; }
        
        public string IpAddress { get; set; }
        
        public int SourcePort { get; set; }
        
        public string Country { get; set; }
    }
}
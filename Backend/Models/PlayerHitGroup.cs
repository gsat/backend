using Microsoft.EntityFrameworkCore.InMemory.Storage.Internal;

namespace Backend.Models
{
    public class PlayerHitGroup
    {
        public int Generic { get; set; }
        
        public int Head { get; set; }
        
        public int Chest { get; set; }
        
        public int Stomach { get; set; }
        
        public int LeftHand { get; set; }
        
        public int RightHand { get; set; }
        
        public int LeftLeg { get; set; }
        
        public int RightLeg { get; set; }
        
        public int Gear { get; set; }
    }
}
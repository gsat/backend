﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.ResponseCaching.Internal;
using Microsoft.Extensions.Logging;

namespace Backend.Services
{
    public class SourceEngine
    {
        private readonly IPAddress mIpAddress;

        private readonly int mPort;

        private readonly string mPassword;

        private readonly TcpClient mTcpClient;

        private NetworkStream mNetworkStream;

        private int mIndexCommand;

        private ILogger mLogger;

        /// <summary>
        ///  
        /// </summary>
        /// <param name="address"></param>
        /// <param name="port"></param>
        /// <param name="password"></param>
        public SourceEngine(string address, int port, string password)
        {
            var ipAddress = Dns.GetHostAddresses(address);

            mTcpClient = new TcpClient();
            mTcpClient.SendTimeout = 5000;
            
            mIpAddress = ipAddress[0];
            mPort = port;
            mPassword = password;

            mLogger = new Logger<SourceEngine>(new LoggerFactory());
        }

        private async Task<bool> ConnectAsync()
        {
            try
            {
                await mTcpClient.ConnectAsync(mIpAddress, mPort).ConfigureAwait(false);
                mNetworkStream = mTcpClient.GetStream();
            }
            catch (Exception ex)
            {
                mLogger.LogError("Error connect to server, " + ex.Message);
                return false;
            }

            return true;
        }

        private async Task<bool> TestConnection()
        {    
            if (mTcpClient.Connected)
                return true;
            
            if (!mTcpClient.Connected)
            {
                // Enable tcp connection
                await ConnectAsync().ConfigureAwait(false);
                
                // Auth
                await WriteAsyncInternal(new byte[] {0x03, 0x00, 0x00, 0x00}, mPassword).ConfigureAwait(false);
                
                // Write command
                var response = await WriteAsyncInternal(new byte[] {0x02, 0x00, 0x00, 0x00}, "echo web: test").ConfigureAwait(false);

                if (response.Contains("web : test"))
                    return true;
            }

            return false;
        }

        public async Task<string> WriteAsync(string body)
        {
            if (!await TestConnection())
                throw new Exception("Error test connection");

            return await WriteAsyncInternal(new byte[] {0x02, 0x00, 0x00, 0x00}, body).ConfigureAwait(false);
        }
        
        private async Task<string> WriteAsyncInternal(byte [] commandType, string body)
        {
            //Debug.WriteLine("Request: " + body);
            
            mIndexCommand++;

            var query = new List<byte>();
                
            // Set command index
            query.AddRange(BitConverter.GetBytes(mIndexCommand));
                
            // Set command type
            query.AddRange(commandType);
                
            // Set body
            query.AddRange(Encoding.ASCII.GetBytes(body));
                
            // Set end line
            query.Add(0x00);
            
            var packetSize = BitConverter.GetBytes(query.Count).ToList();
            var packet = packetSize.Concat(query).ToArray();
            
            await mNetworkStream.WriteAsync(packet, 0, packet.Length);
            
            var result = await ReadAsyncInternal();
            
            return result;
        }

        private async Task<string> ReadAsyncInternal()
        {
            Tuple<int, List<byte>> result;
            do
            {
                result = await GetData().ConfigureAwait(false);
            } while (result.Item1 != mIndexCommand);

            return Encoding.UTF8.GetString(result.Item2.ToArray()).Trim('\0');
        }

        private async Task<Tuple<int, List<byte>>> GetData()
        {
            var buffer = new List<byte>();
            var singleByte = new byte [4];

            do
            {
                Thread.Sleep(10);
            } while (!mNetworkStream.DataAvailable);
            
            do
            {
                await mNetworkStream.ReadAsync(singleByte, 0, singleByte.Length).ConfigureAwait(false);
                buffer.AddRange(singleByte);
                
            } while (mNetworkStream.DataAvailable);
            
            //Debug.WriteLine($"Response {mIndexCommand}: " + BitConverter.ToString(buffer.ToArray()));
            
            int reqSize;
            int reqId;
            List<byte> body;

            var buff = buffer;
            bool isEnd = false;
            do
            {
                reqSize = BitConverter.ToInt32(buff.Take(4).ToArray());
                reqId = BitConverter.ToInt32(buff.Skip(4).Take(4).ToArray());
                body = buff.Skip(8).Take(reqSize - 8).ToList();
                
                buff = buff.Skip(4 + reqSize).ToList();

                if (!buff.Any() || buff.Count < 4)
                    isEnd = true;
                
            } while (!isEnd);

            return new Tuple<int, List<byte>>(reqId, body);
        }

        public void CloseConnection()
        {
            mNetworkStream.Close();
            mTcpClient.Close();
        }
    }
}

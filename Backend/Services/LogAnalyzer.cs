using System;
using System.Text.RegularExpressions;
using Backend.Models.PlayerActions;

namespace Backend.Services
{
    public class LogAnalyzer
    {
        /// <summary>
        ///  Example: "Player<USERID><UUID><TEAM>" connected, address "192.168.0.2:27005"
        /// </summary>
        private readonly Regex mRegexConnected;

        /// <summary>
        ///  Example: "Player<USERID><UUID><TEAM>" entered the game
        /// </summary>
        private readonly Regex mRegexEntered;

        /// <summary>
        ///  Example: "Player<USERID><UUID><TEAM>" disconnected (reason "Disconnect by user.")
        /// </summary>
        private readonly Regex mRegexDisconnected;

        /// <summary>
        ///  Example: "Player<USERID><UUID><TEAM>" say "ff"
        /// </summary>
        private readonly Regex mRegexSay;

        /// <summary>
        ///  Example: "Player1<USERID><UUID><TEAM>" attacks "Player2<USERID><UUID><TEAM>"
        ///           damage <GENERIC><HEAD><CHEST><STOMACH><LHAND|RHAND><LLEG|RLEG><GEAR>
        ///           armor <GENERIC><HEAD><CHEST><STOMACH><LHAND|RHAND><LLEG|RLEG><GEAR>
        /// </summary>
        private readonly Regex mRegexHurt;
        private readonly Regex mRegexDamage;
        private readonly Regex mRegexDamageArmor;
        
        /// <summary>
        ///  Example: "Player1<USERID><UUID><TEAM>" killed "Player2<USERID><UUID><TEAM>" with "WEAPON_NAME"
        /// </summary>
        private readonly Regex mRegexKill;
        
        public LogAnalyzer()
        {
            mRegexConnected = new Regex("\"(?<name>.*)<(?<userid>\\d+)><><.*>\"\\sconnected,\\saddress\\s\"(?<address>[\\d]{1,3}.[\\d]{1,3}.[\\d]{1,3}.[\\d]{1,3}):(?<port>\\d+)\"\\sfrom\\s<(?<country>.*)>");
            mRegexEntered = new Regex("(?<name>.*)<(?<userid>\\d+)><(?<uuid>.*)><.*>\"\\sentered\\sthe\\sgame");
            mRegexDisconnected = new Regex("\"(?<name>.*)<(?<userid>\\d+)><(?<uuid>.*)><.*>\"\\sdisconnected\\s\\(reason\\s\"(?<reason>.*)\"\\)");
            mRegexSay = new Regex("\"(?<name>.*)<(?<userid>\\d+)><(?<uuid>.*)><.*>\"\\ssay\\s\"(?<message>.*)\"");
            
            // Damage event
            mRegexHurt = new Regex("\"(?<srcName>.*)<(?<srcUserid>\\d+)><(?<srcUuid>.*)><.*>\"\\sattacks\\s\"(?<dstName>.*)<(?<dstUserid>\\d+)><(?<dstUuid>.*)><.*>\"");
            mRegexDamage = new Regex("damage\\s<(?<generic>\\d+)><(?<head>\\d+)><(?<chest>\\d+)><(?<stomach>\\d+)><(?<leftHand>\\d+)\\|(?<rightHand>\\d+)><(?<leftLeg>\\d+)\\|(?<rightLeg>\\d+)><(?<gear>\\d+)>");
            mRegexDamageArmor = new Regex("armor\\s<(?<generic>\\d+)><(?<head>\\d+)><(?<chest>\\d+)><(?<stomach>\\d+)><(?<leftHand>\\d+)\\|(?<rightHand>\\d+)><(?<leftLeg>\\d+)\\|(?<rightLeg>\\d+)><(?<gear>\\d+)>");
            
            mRegexKill = new Regex("\"(?<srcName>.*)<(?<srcUserid>\\d+)><(?<srcUuid>.*)><.*>\"\\skilled\\s\"(?<dstName>.*)<(?<dstUserid>\\d+)><(?<dstUuid>.*)><.*>\"\\swith\\s\"(?<weapon>.*)\"");
        }

        public object Analyze(string row)
        {
            if (mRegexConnected.IsMatch(row))
                return MapActionConnect(mRegexConnected.Match(row));

            if (mRegexEntered.IsMatch(row))
                return MapActionEntered(mRegexEntered.Match(row));
            
            if (mRegexDisconnected.IsMatch(row))
                return MapActionDisconnect(mRegexDisconnected.Match(row));
            
            if (mRegexSay.IsMatch(row))
                return MapActionSay(mRegexSay.Match(row));

            if (mRegexHurt.IsMatch(row))
                return MapActionHurt(mRegexHurt.Match(row), mRegexDamage.Match(row), mRegexDamageArmor.Match(row));
            
            if (mRegexKill.IsMatch(row))
                return MapActionKill(mRegexKill.Match(row));
            
            //Console.WriteLine("Unknown row: " + row);
            return null;
        }

        private Connect MapActionConnect(Match match)
        {
            return new Connect
            {
                Name = match.Groups["name"].Value,
                UserId = int.Parse(match.Groups["userid"].Value),
                IpAddress = match.Groups["address"].Value,
                SourcePort = int.Parse(match.Groups["port"].Value),
                Country = match.Groups["country"].Value
            };
        }

        private object MapActionEntered(Match match)
        {
            return new Entered
            {
                Name = match.Groups["name"].Value,
                UserId = int.Parse(match.Groups["userid"].Value),
                Guid = Guid.Parse(match.Groups["uuid"].Value)
            };
        }

        private Disconnect MapActionDisconnect(Match match)
        {
            return new Disconnect
            {
                Name = match.Groups["name"].Value,
                UserId = int.Parse(match.Groups["userid"].Value),
                Reason = match.Groups["reason"].Value
            };
        }

        private Say MapActionSay(Match match)
        {
            return new Say
            {
                Name = match.Groups["name"].Value,
                UserId = int.Parse(match.Groups["userid"].Value),
                Message = match.Groups["message"].Value
            };
        }

        private object MapActionHurt(Match match, Match damage, Match armorDamage)
        {
            return new Hurt
            {
                SrcName = match.Groups["srcName"].Value,
                SrcUserId = int.Parse(match.Groups["srcUserid"].Value),
                DstName = match.Groups["dstName"].Value,
                DstUserId = int.Parse(match.Groups["dstUserid"].Value),
                Generic = int.Parse(damage.Groups["generic"].Value),
                GenericArmor = int.Parse(armorDamage.Groups["generic"].Value),
                Head = int.Parse(damage.Groups["head"].Value),
                HeadArmor = int.Parse(armorDamage.Groups["head"].Value),
                Chest = int.Parse(damage.Groups["chest"].Value),
                ChestArmor = int.Parse(armorDamage.Groups["chest"].Value),
                Stomach = int.Parse(damage.Groups["stomach"].Value),
                StomachArmor = int.Parse(armorDamage.Groups["stomach"].Value),
                LeftHand = int.Parse(damage.Groups["leftHand"].Value),
                LeftHandArmor = int.Parse(armorDamage.Groups["leftHand"].Value),
                RightHand = int.Parse(damage.Groups["rightHand"].Value),
                RightHandArmor = int.Parse(armorDamage.Groups["rightHand"].Value),
                LeftLeg = int.Parse(damage.Groups["leftLeg"].Value),
                LeftLegArmor = int.Parse(armorDamage.Groups["leftLeg"].Value),
                RightLeg = int.Parse(damage.Groups["rightLeg"].Value),
                RightLegArmor = int.Parse(armorDamage.Groups["rightLeg"].Value),
                Gear = int.Parse(damage.Groups["gear"].Value),
                GearArmor = int.Parse(armorDamage.Groups["gear"].Value)
            };
        }

        private Kill MapActionKill(Match match)
        {
            return new Kill
            {
                SrcName = match.Groups["srcName"].Value,
                SrcUserId = int.Parse(match.Groups["srcUserid"].Value),
                DstName = match.Groups["dstName"].Value,
                DstUserId = int.Parse(match.Groups["dstUserid"].Value),
                WithWeapon = match.Groups["weapon"].Value
            };
        }
    }
}
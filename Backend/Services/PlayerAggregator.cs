using System;
using System.Collections.Generic;
using Backend.Models;
using Backend.Models.PlayerActions;

namespace Backend.Services
{
    public class PlayerAggregator
    {
        private readonly PlayerStorage mPlayerStorage;

        public PlayerAggregator(PlayerStorage playerStorage)
        {
            mPlayerStorage = playerStorage;
        }

        public void Aggregate(object data)
        {
            if (data == null)
                return;

            if (data.GetType() == typeof(Connect))
            {
                ProcessConnect((Connect) data);
                return;
            }

            if (data.GetType() == typeof(Entered))
            {
                ProcessEntered((Entered) data);
                return;
            }

            if (data.GetType() == typeof(Disconnect))
            {
                ProcessDisconnect((Disconnect) data);
                return;
            }

            if (data.GetType() == typeof(Say))
            {
                ProcessSay((Say) data);
                return;
            }

            if (data.GetType() == typeof(Hurt))
            {
                ProcessHurt((Hurt) data);
                return;
            }

            if (data.GetType() == typeof(Kill))
            {
                ProcessKill((Kill) data);
                return;
            }
        }

        public void BasicInit(List<Player> players)
        {
            foreach (var player in players)
            {
                mPlayerStorage.AddPlayer(player.UserId, new PlayerInfo
                {
                    UserId = player.UserId,
                    Name = player.UserName,
                    Connected = DateTime.Now.AddSeconds(-player.PlayTime.TotalSeconds),
                    Disconnected = null,
                    SessionTime = null,
                    Ping = player.Ping,
                    Loss = player.Loss,
                    IpAddress = player.Ip,
                    Port = player.Port
                    //Kills = ,
                    //Deaths = ,
                    //Country = 
                });
            }
        }

        private void ProcessConnect(Connect connect)
        {
            mPlayerStorage.AddPlayer(connect.UserId, new PlayerInfo
            {
                UserId = connect.UserId,
                Name = connect.Name,
                Connected = DateTime.Now,
                Disconnected = null,
                SessionTime = null,
                //Ping = ,
                //Loss = ,
                IpAddress = connect.IpAddress,
                Port = connect.SourcePort,
                //Kills = ,
                //Deaths = ,
                Country = connect.Country
            });
        }

        private void ProcessEntered(Entered entered)
        {
            mPlayerStorage.SetGuid(entered.UserId, entered.Guid);
        }

        private void ProcessDisconnect(Disconnect disconnect)
        {
            mPlayerStorage.RemovePlayer(disconnect.UserId);
        }

        private void ProcessSay(Say say)
        {
            mPlayerStorage.AddMessage(new PlayerSay
            {
                UserId = say.UserId,
                Name = say.Name,
                Message = say.Message
            });
        }

        private void ProcessHurt(Hurt hurt)
        {
            mPlayerStorage.AddHurt(hurt.SrcUserId,
                    hurt.Generic,
                    hurt.GenericArmor,
                    hurt.Head,
                    hurt.HeadArmor,
                    hurt.Chest,
                    hurt.ChestArmor,
                    hurt.Stomach,
                    hurt.StomachArmor,
                    hurt.LeftHand,
                    hurt.LeftHandArmor,
                    hurt.RightHand,
                    hurt.RightHandArmor,
                    hurt.LeftLeg,
                    hurt.LeftLegArmor,
                    hurt.RightLeg,
                    hurt.RightLegArmor,
                    hurt.Gear,
                    hurt.GearArmor);
        }

        private void ProcessKill(Kill kill)
        {
            mPlayerStorage.AddKill(kill.SrcUserId);
            mPlayerStorage.AddDeath(kill.DstUserId);
        }
    }
}
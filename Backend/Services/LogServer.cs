using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Backend.Models;

namespace Backend.Services
{
    public class LogServer : IHostedService
    {
        private readonly LogAnalyzer mLogAnalyzer;
        
        private readonly PlayerAggregator mPlayerAggregator;

        private readonly SourceQuery mSourceQuery;
        
        private readonly UdpClient mLogServer;

        private IPEndPoint mIpEndpoint;

        private readonly ILogger mLogger;
        
        public LogServer(LogAnalyzer logAnalyzer, PlayerAggregator playerAggregator, SourceQuery sourceQuery, ILogger<LogServer> logger)
        {
            mLogAnalyzer = logAnalyzer;
            mPlayerAggregator = playerAggregator;
            
            mSourceQuery = sourceQuery;
            mSourceQuery.SetConnectionInfo("server-hostname.com", 27015, "password");
            
            mLogServer = new UdpClient(27500);

            mLogger = logger;
        }
        
        public virtual Task StartAsync(CancellationToken cancellationToken)
        {
            mIpEndpoint = new IPEndPoint(IPAddress.Any, 27500);

            Task.Run(async () =>
            {
                mLogger.LogInformation("Started log server");
                
                var players = await mSourceQuery.GetPlayers().ConfigureAwait(false);
                mPlayerAggregator.BasicInit(players.ToList());
                
                while (!cancellationToken.IsCancellationRequested)
                {
                    try
                    {
                        byte[] bytes = mLogServer.Receive(ref mIpEndpoint);
                        var message = Encoding.UTF8.GetString(bytes);

                        var result = mLogAnalyzer.Analyze(message);
                        mPlayerAggregator.Aggregate(result);
                    }
                    catch (Exception ex)
                    {
                        mLogger.LogError("Log server shutdown, " + ex.Message);
                        break;
                    }
                }
            });

            return Task.CompletedTask;
        }

        public virtual Task StopAsync(CancellationToken cancellationToken)
        {
            mLogServer.Close();
            return Task.CompletedTask;
        }
    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using Backend.Models;

namespace Backend.Services
{
    public class PlayerStorage
    {
        private Dictionary<int, PlayerInfo> mConnectedCollection;

        private List<PlayerInfo> mDisconnectedCollection;

        private Dictionary<DateTime, PlayerSay> mMessageCollection;

        public PlayerStorage()
        {
            mConnectedCollection = new Dictionary<int, PlayerInfo>();
            mDisconnectedCollection = new List<PlayerInfo>();
            
            mMessageCollection = new Dictionary<DateTime, PlayerSay>();
        }

        public Dictionary<int, PlayerInfo> GetConnectedPlayers()
        {
            foreach (var player in mConnectedCollection)
                player.Value.SessionTime = DateTime.Now - player.Value.Connected;

            return mConnectedCollection;
        }

        public List<PlayerInfo> GetDisconnectedPlayers()
        {
            return mDisconnectedCollection;
        }

        public Dictionary<DateTime, PlayerSay> GetMessages()
        {
            return mMessageCollection;
        }

        public void AddPlayer(int userId, PlayerInfo playerInfo)
        {
            // Fake client check (is a bot)
            if (mConnectedCollection.ContainsKey(userId))
                return;
            
            mConnectedCollection.Add(userId, playerInfo);
        }

        public void RemovePlayer(int userId)
        {
            // Fake client check (is a bot)
            if (!mConnectedCollection.ContainsKey(userId))
                return;

            // Remove disconnected older than 3 hours
            var doRemove = new List<PlayerInfo>();
            foreach (var item in mDisconnectedCollection)
            {
                var diff = DateTime.Now - item.Disconnected;

                if (diff != null && diff.Value.Hours > 6)
                    doRemove.Add(item);
            }

            foreach (var item in doRemove)
                mDisconnectedCollection.Remove(item);
            
            // Get player info
            var player = mConnectedCollection[userId];
            
            // Set time disconnected
            player.Disconnected = DateTime.Now;
            
            // Set session time
            player.SessionTime = player.Disconnected - player.Connected;

            // Store disconnected player
            mDisconnectedCollection.Add(player);
            
            // Remove connected player
            mConnectedCollection.Remove(userId);
        }

        public void AddMessage(PlayerSay playerSay)
        {
            if (mMessageCollection.Count > 40)
                mMessageCollection.Remove(mMessageCollection.Keys.First());
            
            mMessageCollection.Add(DateTime.Now, playerSay);
        }

        public void AddHurt(int userId,
                int generic, int genericArmor,
                int head, int headArmor,
                int chest, int chestArmor,
                int stomach, int stomachArmor,
                int leftHand, int leftHandArmor,
                int rightHand, int rightHandArmor,
                int leftLeg, int leftLegArmor,
                int rightLeg, int rightLegArmor,
                int gear, int gearArmor)
        {
            if (mConnectedCollection.ContainsKey(userId))
            {
                if (generic > 0 || genericArmor > 0)
                    mConnectedCollection[userId].HitGroup.Generic++;

                if (head > 0 || headArmor > 0)
                    mConnectedCollection[userId].HitGroup.Head++;

                if (chest > 0 || chestArmor > 0)
                    mConnectedCollection[userId].HitGroup.Chest++;

                if (stomach > 0 || stomachArmor > 0)
                    mConnectedCollection[userId].HitGroup.Stomach++;

                if (leftHand > 0 || leftHandArmor > 0)
                    mConnectedCollection[userId].HitGroup.LeftHand++;
                
                if (rightHand > 0 || rightHandArmor > 0)
                    mConnectedCollection[userId].HitGroup.RightHand++;

                if (leftLeg > 0 || leftLegArmor > 0)
                    mConnectedCollection[userId].HitGroup.LeftLeg++;

                if (rightLeg > 0 || rightLegArmor > 0)
                    mConnectedCollection[userId].HitGroup.RightLeg++;

                if (gear > 0 || gearArmor > 0)
                    mConnectedCollection[userId].HitGroup.Gear++;
            }
        }

        public void AddKill(int userId)
        {
            // Fake client check (is a bot)
            if (mConnectedCollection.ContainsKey(userId))
                mConnectedCollection[userId].Kills++;
        }

        public void AddDeath(int userId)
        {
            // Fake client check (is a bot)
            if (mConnectedCollection.ContainsKey(userId))
                mConnectedCollection[userId].Deaths++;
        }

        public void SetGuid(int userId, Guid guid)
        {
            mConnectedCollection[userId].Guid = guid;
        }
    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Backend.Models;

namespace Backend.Services
{
    public class SourceQuery
    {
        private SourceEngine mSourceEngine;
        
        private readonly Regex mStatusPattern;

        private readonly Regex mMapsPattern;

        public SourceQuery()
        {
            mStatusPattern = new Regex(
                    "\\s?(?<userid>\\d+)\\s\"(?<username>.*)\"\\sSTEAM_ID_PENDING\\s+((?<hours>\\d+):)?(?<minutes>\\d+):(?<seconds>\\d+)\\s(?<ping>\\d+)\\s(?<loss>\\d+)\\s.*\\s(?<ip>[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}):(?<port>\\d+)");
            
            mMapsPattern = new Regex(
                    "\\(fs\\)\\s(?<map>.*)\\.bsp");
        }

        public void SetConnectionInfo(string address, int port, string password)
        {
            mSourceEngine = new SourceEngine(address, port, password);
        }

        public async Task<IEnumerable<Player>> GetPlayers()
        {
            var players = new List<Player>();

            var result = await mSourceEngine.WriteAsync("status");
            mSourceEngine.CloseConnection();
            
            var matches = mStatusPattern.Matches(result);

            foreach (Match m in matches)
            {
                var hours = m.Groups["hours"].Success ? int.Parse(m.Groups["hours"].Value) : 0;
                var minutes = m.Groups["minutes"].Success ? int.Parse(m.Groups["minutes"].Value) : 0;
                var seconds = m.Groups["seconds"].Success ? int.Parse(m.Groups["seconds"].Value) : 0;

                var time = new TimeSpan()
                        .Add(TimeSpan.FromHours(hours))
                        .Add(TimeSpan.FromMinutes(minutes))
                        .Add(TimeSpan.FromSeconds(seconds));

                players.Add(new Player
                {
                    UserId = int.Parse(m.Groups["userid"].Value),
                    UserName = m.Groups["username"].Value,
                    PlayTime = time,
                    Ping = int.Parse(m.Groups["ping"].Value),
                    Loss = int.Parse(m.Groups["loss"].Value),
                    Ip = m.Groups["ip"].Value,
                    Port = int.Parse(m.Groups["port"].Value)
                });
            }

            return players.OrderBy(x => x.PlayTime);
        }

        public async Task<MapCollections> GetMaps()
        {
            var mapCollections = new MapCollections();
            
            var result = await mSourceEngine.WriteAsync("maps *");
            mSourceEngine.CloseConnection();
            
            var matches = mMapsPattern.Matches(result);

            foreach (Match m in matches)
            {
                var map = m.Groups["map"].Value;

                if (map.Contains("awp_"))
                    mapCollections.SniperRifle.Add(map.Replace("awp_", ""));
                else if (map.Contains("de_"))
                    mapCollections.BombDefusal.Add(map.Replace("de_", ""));
                else if (map.Contains("cs_"))
                    mapCollections.HostageRescue.Add(map.Replace("cs_", ""));
                else
                    mapCollections.Another.Add(map);
            }

            return mapCollections;
        }

        public async Task ChangeLevel(string map)
        {
            await mSourceEngine.WriteAsync($"changelevel {map}");
            mSourceEngine.CloseConnection();
        }

        public async Task BanPlayer(string ip, int time)
        {
            await mSourceEngine.WriteAsync($"addip {time} {ip}");
            mSourceEngine.CloseConnection();
        }

        public async Task KickPlayer(string userId)
        {
            await mSourceEngine.WriteAsync($"kickid {userId}");
            mSourceEngine.CloseConnection();
        }

        public async Task Command(string command, string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                await mSourceEngine.WriteAsync($"{command} {value}");
                return;
            }
            
            // Set without value
            await mSourceEngine.WriteAsync($"{command}");
        }
    }
}
﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Backend.Models;
using Backend.Services;
using Microsoft.AspNetCore.Mvc;

namespace Backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ServerController : ControllerBase
    {
        private readonly PlayerStorage mPlayerStorage;
        
        private readonly SourceQuery mSourceQuery;
        
        public ServerController(PlayerStorage playerStorage)
        {
            mPlayerStorage = playerStorage;
            
            mSourceQuery = new SourceQuery();
            mSourceQuery.SetConnectionInfo("server-hostname.com", 27015, "password");
        }
        
        // GET api/server
        [HttpGet]
        public IEnumerable<PlayerInfo> Get()
        {
            //return await mSourceQuery.GetPlayers();
            return mPlayerStorage.GetConnectedPlayers().Values.OrderBy(x => x.SessionTime);
        }

        [HttpGet("disconnected")]
        public IEnumerable<PlayerInfo> Disconnected()
        {
            var disconnectedPlayers = mPlayerStorage.GetDisconnectedPlayers();
            return disconnectedPlayers.OrderByDescending(x => x.Disconnected);
        }

        [HttpGet("messages")]
        public object Messages()
        {
            return mPlayerStorage.GetMessages();
        }

        // GET api/server/maps
        [HttpGet("maps")]
        public async Task<ActionResult<MapCollections>> Maps()
        {
            return await mSourceQuery.GetMaps();
        }
        
        // POST api/server/changelevel
        [HttpPost("changelevel/{map}")]
        public async Task<ActionResult<object>> ChangeLevel(string map)
        {
            await mSourceQuery.ChangeLevel(map);

            return new[]
            {
                "ok"
            };
        }

        [HttpPost("ban/{ip}/{time:int}")]
        public async Task<ActionResult<object>> BanPlayer(string ip, int time)
        {
            await mSourceQuery.BanPlayer(ip, time);
            
            return new[]
            {
                "ok"
            };
        }
        
        [HttpPost("kick/{userId}")]
        public async Task<ActionResult<object>> KickPlayer(string userId)
        {
            await mSourceQuery.KickPlayer(userId);
            
            return new[]
            {
                "ok"
            };
        }

        [HttpPost("command/{command}/{value?}")]
        public async Task<ActionResult<object>> Command(string command, string value)
        {
            await mSourceQuery.Command(command, value);
            
            return new[]
            {
                "ok"
            };
        }
    }
}
